const forrestController = require('../../controllers/api/v1/ForrestController');

const koaRouter = require('koa-router');
const forrestRouter = new koaRouter();

forrestRouter.get('/forrest', forrestController.index);
forrestRouter.post('/forrest', forrestController.store);
forrestRouter.delete('/forrest/:id', forrestController.delete);

forrestRouter.get('/forrest/test', async (ctx) => {
  ctx.body = 'forrest msg from aws';
});

module.exports = forrestRouter;
