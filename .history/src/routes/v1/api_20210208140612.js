const userController = require('../../controllers/api/v1/user');

const koaRouter = require('koa-router');
const router = new koaRouter();

router.get('/users', userController.index);
router.post('/users', userController.store);
router.delete('/users/:id', userController.delete);
router.get('/users/:id', userController.show);
router.put('/users/:id', userController.update);

router.get('/', async (ctx) => {
  ctx.body = 'easy-grade homepage coming soon!';
});

router.get('/chaokai', async (ctx) => {
  ctx.body = 'chaokai msg from aws';
});

router.get('/zhian', async (ctx) => {
  ctx.body = 'zhian msg from aws';
});

router.get('/shawn', async (ctx) => {
  ctx.body = 'shawn msg from aws';
});

router.get('/jin', async (ctx) => {
  ctx.body = 'jin msg from aws';
});

router.get('/ben', async (ctx) => {
  ctx.body = 'ben msg from aws';
});
module.exports = router;
