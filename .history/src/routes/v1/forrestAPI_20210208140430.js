const forrestController = require('../../controllers/api/v1/ForrestController');

const koaRouter = require('koa-router');
const forrestRouter = new koaRouter();

linRouter.get('/lin', linController.index);
linRouter.post('/lin', linController.store);
linRouter.delete('/lin/:id', linController.delete);

linRouter.get('/lin/test', async (ctx) => {
  ctx.body = 'lin msg from aws';
});

module.exports = linRouter;
