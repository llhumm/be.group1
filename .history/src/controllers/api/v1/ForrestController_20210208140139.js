const UserForrest = require('../../../model/ForrestDB');

exports.index = async (ctx) => {
  try {
    await UserForrest.find({}, (err, users) => {
      ctx.body = users;
    });
  } catch (e) {
    ctx.body = e;
  }
};

exports.store = async (ctx) => {
  const forrest = new UserForrest(ctx.request.body);
  try {
    await forrest.save();
    const token = 'save to mongoDB: successful!!';
    ctx.status(201).send({ lin, token });
  } catch (e) {
    ctx.body = e;
  }
};

exports.show = async (ctx) => {
  try {
    await UserLin.findById(ctx.params.id, (err, user) => {
      ctx.body = user;
    });
  } catch (e) {
    ctx.body = e;
  }
};

exports.delete = async (ctx) => {
  try {
    await UserLin.findByIdAndRemove(ctx.params.id, () => {
      ctx.body = 'Successfully delete user id: ' + ctx.params.id;
    });
  } catch (e) {
    ctx.body = e;
  }
};

exports.update = async (ctx) => {
  try {
    await UserLin.findByIdAndUpdate(ctx.params.id, ctx.request.body, { rawResult: true }, () => {
      ctx.body = 'Successfully update user id: ' + ctx.params.id;
    });
  } catch (e) {
    ctx.body = e;
  }
};
