const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    // firstName: {type: String, default: "", trim: true},
    // lastName: {type: String, default: "", trim: true},
  },
  email: {
    type: String,
    unique: true,
    required: true, //When a field is mandatory to be filled then in that case we mention it as required.
    trim: true, //Removes whitespace characters, including null, or the specified characters from the beginning and end of a string.
    // index: true,
  },
  mobile: {
    type: String,
  },
  id: {
    type: String,
    unique: true,
  },
});

const LinDB = mongoose.model('Lin', userSchema);
module.exports = LinDB;
