const cors = require('koa-cors');
const apiRouter = require('../../src/routes/v1/api');

const linAPIRouter = require('../../src/routes/v1/linAPI');

const config = require('../../src/config/app');

module.exports = async (app) => {
  app.use(cors());
  app.use(apiRouter.routes());
  //add lin api routes
  app.use(linAPIRouter.routes());
  app.use(linAPIRouter.routes());

  return app;
};
